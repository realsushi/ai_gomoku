"""read_stdin
get_line()
"""
"""
#### input templates
* START size
* TURN [X],[Y]
    Example:
    The manager sends:
    TURN 10,10
    The brain answers:
    11,10

* BEGIN
    for the first player
    Example:
    The manager sends:
    BEGIN
    The brain answers:
    10,10

* BOARD
    Example:
    The manager sends:
    BOARD
    10,10,1 [allied]
    10,11,2 [enemy]
    11,11,1
    9,10,2
    DONE
    The brain answers:
    9,9

* INFO [key] [value]
    The key can be:
    timeout_turn  - time limit for each move (milliseconds, 0=play as fast as possible)
    timeout_match - time limit of a whole match (milliseconds, 0=no limit)
    max_memory    - memory limit (bytes, 0=no limit)
    time_left     - remaining time limit of a whole match (milliseconds)
    game_type     - 0=opponent is human, 1=opponent is brain, 2=tournament, 3=network tournament
    rule          - bitmask or sum of 1=exactly five in a row win, 2=continuous game, 4=renju
    evaluate      - coordinates X,Y representing current position of the mouse cursor
    folder        - folder for persistent files

    Example:
    INFO timeout_match 300000
    INFO timeout_turn 10000
    INFO max_memory 83886080
    
    Expected answer: none

* END
* ABOUT
The brain is expected to send some information about itself on one line. Each info must be written as keyword, equals sign, text value in quotation marks. Recommended keywords are name, version, author, country, www, email. Values should be separated by commas that can be followed by spaces. The manager can use this info, but must cope with old brains that used to send only human-readable text.

Example:
 The manager sends:
  ABOUT
 The brain answers:
  name="SomeBrain", version="1.0", author="Nymand", country="USA"

"""